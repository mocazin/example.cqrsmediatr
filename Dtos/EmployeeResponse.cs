﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace example.CqrsMediatR.Dtos
{
    public class EmployeeResponse
    {
        public Guid Id { get; set; }
        public string EmployeeNumber { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime CreatedDated { get; set; }
        public string CreatedBy { get; set; }
    }
}
