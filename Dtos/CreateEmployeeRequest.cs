﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace example.CqrsMediatR.Dtos
{
    public class CreateEmployeeRequest
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
