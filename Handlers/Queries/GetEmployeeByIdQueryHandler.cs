﻿using AutoMapper;
using example.CqrsMediatR.Dtos;
using example.CqrsMediatR.Infrastructure;
using Example.CqrsMediatR.Controllers._2._0;
using Example.CqrsMediatR.Infrastructure;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Example.CqrsMediatR.Handlers.Queries
{
    public class GetEmployeeByIdQueryHandler : IRequestHandler<GetEmployeeByIdQuery, EmployeeResponse>
    {
        private readonly IMongoDataStore dataStore;
        private readonly IMapper mapper;

        public GetEmployeeByIdQueryHandler(IMongoDataStore dataStore, IMapper mapper)
        {
            this.dataStore = dataStore;
            this.mapper = mapper;
        }
        public async Task<EmployeeResponse> Handle(GetEmployeeByIdQuery request, CancellationToken cancellationToken)
        {
            var employee = await dataStore.GetByIdAsync(request.Id);
            var result = mapper.Map<EmployeeResponse>(employee);
            return result;
        }
    }
}
