﻿using AutoMapper;
using example.CqrsMediatR.Dtos;
using Example.CqrsMediatR.Infrastructure;
using Example.CqrsMediatR.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Example.CqrsMediatR.Handlers.Queries
{
    public class GetAllEmployeesQueryHandler : IRequestHandler<GetAllEmployeesQuery, IEnumerable<EmployeeResponse>>
    {
        private readonly IMongoDataStore dataStore;
        private readonly IMapper mapper;

        public GetAllEmployeesQueryHandler(IMongoDataStore dataStore, IMapper mapper)
        {
            this.dataStore = dataStore;
            this.mapper = mapper;
        }

        public async Task<IEnumerable<EmployeeResponse>> Handle(GetAllEmployeesQuery request, CancellationToken cancellationToken)
        {
            var employees = await dataStore.GetAllAsync();
            var result = mapper.Map<IEnumerable<EmployeeResponse>>(employees);
            return result;
        }
    }
}
