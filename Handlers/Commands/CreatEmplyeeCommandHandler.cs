﻿using example.CqrsMediatR.Infrastructure;
using Example.CqrsMediatR.Commands;
using Example.CqrsMediatR.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Example.CqrsMediatR.Handlers.Commands
{
    public class CreatEmplyeeCommandHandler : IRequestHandler<CreatEmplyeeCommand, Guid>
    {
        private readonly ISqlDataStore dataStore;

        public CreatEmplyeeCommandHandler(ISqlDataStore dataStore)
        {
            this.dataStore = dataStore;
        }
        public async Task<Guid> Handle(CreatEmplyeeCommand request, CancellationToken cancellationToken)
        {
            if (request == null)
                throw new ArgumentNullException();

            var employee = Employee.CreateEmployee(request.Name, request.Surname);
            await dataStore.SaveAsync(employee);
            return employee.Id;
        }
    }
}
