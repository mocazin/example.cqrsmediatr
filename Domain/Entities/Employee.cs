﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example.CqrsMediatR.Domain.Entities
{
    public class Employee
    {
        private readonly static Random rnd = new Random();
        public Guid Id { get; set; }
        public string EmployeeNumber { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public DateTime CreatedDated { get; private set; }
        public string CreatedBy { get; private set; }

        private Employee() { }

        public static Employee CreateEmployee(string name, string surname)
        {
            var employee = new Employee
            {
                Id = Guid.NewGuid(),
                Name = name,
                Surname = surname,
                EmployeeNumber = "Emp" + rnd.Next(500, 999),
                CreatedDated = DateTime.Now,
                CreatedBy = "SomeUser"
            };
            return employee;
        }
    }
}
