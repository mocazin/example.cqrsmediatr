﻿using MediatR;
using System;

namespace Example.CqrsMediatR.Commands
{
    public class CreatEmplyeeCommand : IRequest<Guid>
    {
        public string Name { get; set; }
        public string Surname { get; set; }

    }
}