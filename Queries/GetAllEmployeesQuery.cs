﻿using example.CqrsMediatR.Dtos;
using MediatR;
using System.Collections.Generic;

namespace Example.CqrsMediatR.Queries
{
    public class GetAllEmployeesQuery : IRequest<IEnumerable<EmployeeResponse>>
    {
        public GetAllEmployeesQuery() {}
    }
}