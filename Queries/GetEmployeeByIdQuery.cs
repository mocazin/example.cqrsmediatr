﻿using example.CqrsMediatR.Dtos;
using MediatR;
using System;

namespace Example.CqrsMediatR.Controllers._2._0
{
    public class GetEmployeeByIdQuery : IRequest<EmployeeResponse>
    {
        public Guid Id { get; }

        public GetEmployeeByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}