﻿using AutoMapper;
using example.CqrsMediatR.Dtos;
using Example.CqrsMediatR.Commands;
using Example.CqrsMediatR.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace example.CqrsMediatR.Infrastructure.AutoMapper
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<Employee, Dtos.EmployeeResponse>();
            CreateMap<CreateEmployeeRequest, CreatEmplyeeCommand>();
            CreateMap<List<Employee>, List<Dtos.EmployeeResponse>>();
        }
    }
}
