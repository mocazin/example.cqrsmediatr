﻿using Example.CqrsMediatR.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example.CqrsMediatR.Infrastructure
{
    public interface IMongoDataStore 
    {
        Task<Employee> GetByIdAsync(Guid id);
        Task<IEnumerable<Employee>> GetAllAsync();
        Task SaveAsync(Employee employee);
    }
}
