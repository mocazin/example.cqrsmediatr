﻿using example.CqrsMediatR.Infrastructure;
using Example.CqrsMediatR.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example.CqrsMediatR.Infrastructure
{
    public class MongoDataStore : IMongoDataStore
    {
        private IList<Employee> employeesInMemory;
        public async Task<Employee> GetByIdAsync(Guid id)
        {
            Console.WriteLine("Query Data From [Mongo DB]");

            await Task.Delay(100);
            InitializeEmployeeInMemoryStore();
            return employeesInMemory.FirstOrDefault(p => p.Id == id);
        }

        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            Console.WriteLine("Query Data From [Mongo DB]");

            await Task.Delay(100);
            InitializeEmployeeInMemoryStore();
            return employeesInMemory;
        }

        public async Task SaveAsync(Employee employee)
        {
            Console.WriteLine("Query Data From [Mongo DB]");

            await Task.Delay(100);
            InitializeEmployeeInMemoryStore();
            employeesInMemory.Add(employee);
        }

        void InitializeEmployeeInMemoryStore()
        {
            if (employeesInMemory != null)
                return;

            employeesInMemory = new List<Employee>();

            for (int i = 1; i <= 10; i++)
            {
                employeesInMemory.Add(Employee.CreateEmployee($"James-{i}", $"Smith-{i}"));
            }
        }
    }
}
