﻿using AutoMapper;
using example.CqrsMediatR.Dtos;
using example.CqrsMediatR.Infrastructure;
using Example.CqrsMediatR.Commands;
using Example.CqrsMediatR.Domain.Entities;
using Example.CqrsMediatR.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example.CqrsMediatR.Controllers._2._0
{
    [Route(BaseRouteWithVersionNumber)]
    [ApiController]
    public class EmployeesController : Controller 
    {
        private const string BaseRouteWithVersionNumber = "api/v2.0/[controller]";
        private readonly IMapper mapper;
        private readonly IMediator mediator;

        public EmployeesController(IMapper mapper, IMediator mediator)
        {
            this.mapper = mapper;
            this.mediator = mediator;
        }

        // GET: api/<controller>
        [HttpGet("{id:guid}", Name = "GetAsyncV2")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            // Implement MediatR
            var getEmployeeById = new GetEmployeeByIdQuery(id);
            var employeeResponse = await mediator.Send(getEmployeeById);
            return employeeResponse == null ? (IActionResult)NotFound() : Ok(employeeResponse);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync() 
        {
            var getAllEmployees = new GetAllEmployeesQuery();
            var result = await mediator.Send(getAllEmployees);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateEmployeeRequest createEmployeeRequest)
        {
            // Implement MediatR
            var createEmployeeCommand = mapper.Map<CreatEmplyeeCommand>(createEmployeeRequest);
            var employeeId = await mediator.Send(createEmployeeCommand);
            return CreatedAtRoute("GetAsyncV2", new { id = employeeId }, null);
        }
    }
}
