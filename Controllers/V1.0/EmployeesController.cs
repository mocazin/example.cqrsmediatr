﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using example.CqrsMediatR.Dtos;
using example.CqrsMediatR.Infrastructure;
using Example.CqrsMediatR.Domain.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace example.CqrsMediatR.Controllers.V1._0
{
    [Route(BaseRouteWithVersionNumber)]
    [ApiController]
    public class EmployeesController : Controller
    {
        private const string BaseRouteWithVersionNumber = "api/v1.0/[controller]";
        private readonly ISqlDataStore repository;
        private readonly IMapper mapper;

        public EmployeesController(ISqlDataStore repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        // GET: api/<controller>
        [HttpGet("{id:guid}", Name = "GetAsync")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var employee = await repository.GetByIdAsync(id);
            var result = mapper.Map<EmployeeResponse>(employee);
            return employee == null ? (IActionResult)NotFound() : Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync() 
        {
            var employees = await repository.GetAllAsync(); 
            var result = mapper.Map<IEnumerable<EmployeeResponse>>(employees); 
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync(CreateEmployeeRequest createEmployeeRequest) 
        {
            var employee = Employee.CreateEmployee(createEmployeeRequest.Name, createEmployeeRequest.Surname);
            await repository.SaveAsync(employee);
            return CreatedAtRoute("GetAsync", new { id = employee.Id }, null);
        }
    }
}
